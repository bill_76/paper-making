<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class RolesAndPermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Reset cached roles and permissions
        app()[\Spatie\Permission\PermissionRegistrar::class]->forgetCachedPermissions();

        // create permissions
        Permission::create(['name' => 'edit report']);
        Permission::create(['name' => 'delete report']);
        Permission::create(['name' => 'submit report']);

        Permission::create(['name' => 'parking-module']);
        Permission::create(['name' => 'reporting-module']);
        Permission::create(['name' => 'timesheet-module']);

        // create roles and assign created permissions

        // this can be done as separate statements
        $role = Role::create(['name' => 'manager']);
        $role->givePermissionTo('submit report');
        $role->givePermissionTo('edit report');

        $role = Role::create(['name' => 'admin']);
        $role->givePermissionTo(Permission::all());
    }
}
