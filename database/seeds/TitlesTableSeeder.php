<?php

use App\Models\Title;
use Illuminate\Database\Seeder;

class TitlesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $titles = [
            'Mr.',
            'Mis.',
            'Dr.',
            'Hon.',
        ];

        foreach ($titles as $title){
            Title::create([
                'title' => $title,
            ]);
        }
    }
}
