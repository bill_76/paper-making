<?php

use App\Models\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = [
            [
                'location' => [
                    'name'           => 'community-131',
                    'street_address' => 'street-11',
                    'zip_code'       => '54000',
                    'state_id'       => 1,
                    'city_id'        => 1,
                ],
                'user' => [
                    'title_id'      => 1,
                    'first_name'    => 'Bilal',
                    'last_name'     => 'Ali',
                    'phone'         => '12354325678',
                    'username'      => 'bilal_ali',
                    'email'         => 'bilal.ali.dev@gmail.com',
                    'password'      => Hash::make('12345678'),
                ]
            ], [
                'location' => [
                    'name'           => 'community-132',
                    'street_address' => 'street-11',
                    'zip_code'       => '58000',
                    'state_id'       => 1,
                    'city_id'        => 2,
                ],
                'user' => [
                    'title_id'      => 1,
                    'first_name'    => 'Bilawal',
                    'last_name'     => 'Muneer',
                    'phone'         => '12354325678',
                    'username'      => 'bill',
                    'email'         => 'bilawal.developer@gmail.com',
                    'password'      => Hash::make('12345678'),
                ]
            ], [
                'location' => [
                    'name'           => 'community-134',
                    'street_address' => 'street-44',
                    'zip_code'       => '54000',
                    'state_id'       => 1,
                    'city_id'        => 2,
                ],
                'user' => [
                    'title_id'  => 1,
                    'first_name'=>'George',
                    'last_name' =>'Barbarino',
                    'phone'     =>'0900786013',
                    'username'  => 'george',
                    'email'     =>'sofi@gmail.com',
                    'password'  => Hash::make('12345678'),
                ]
            ],
        ];

        foreach ($users as $user){

//            $location   = Location::create($user['location']);
            $user_      = User::create($user['user']);

            if($user_->username == 'bill'){
                // Adding permissions via a role
                $user_->assignRole('manager');
                $user_->assignRole('admin');
            }else if($user_->username == 'george'){
                // Adding permissions via a role
//                $user_->assignRole('manager');
                $user_->assignRole('admin');
            }else{
                // Adding permissions via a role
                $user_->assignRole('manager');
            }
        }
    }
}
