<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateQuestionAnswersTable extends Migration {

	public function up()
	{
		Schema::create('question_answers', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('user_id')->unsigned()->index();
			$table->integer('paper_category_id')->unsigned()->index();
			$table->integer('question_id')->unsigned()->index();
			$table->longText('ans')->nullable();
			$table->string('img')->nullable();
			$table->boolean('status')->default(1);
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('question_answers');
	}
}