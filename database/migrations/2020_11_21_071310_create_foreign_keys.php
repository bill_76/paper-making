<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Schema;

class CreateForeignKeys extends Migration {

	public function up()
	{
        Schema::table('users', function(Blueprint $table) {
            $table->foreign('title_id')->references('id')->on('titles')
                ->onDelete('cascade')
                ->onUpdate('restrict');
            $table->foreign('state_id')->references('id')->on('states')
                ->onDelete('cascade')
                ->onUpdate('restrict');
            $table->foreign('city_id')->references('id')->on('cities')
                ->onDelete('cascade')
                ->onUpdate('restrict');
        });
		Schema::table('classes', function(Blueprint $table) {
			$table->foreign('user_id')->references('id')->on('users')
						->onDelete('restrict')
						->onUpdate('restrict');
		});
		Schema::table('subjects', function(Blueprint $table) {
			$table->foreign('user_id')->references('id')->on('users')
						->onDelete('restrict')
						->onUpdate('restrict');
		});
		Schema::table('subjects', function(Blueprint $table) {
			$table->foreign('class_id')->references('id')->on('classes')
						->onDelete('restrict')
						->onUpdate('restrict');
		});
		Schema::table('chapters', function(Blueprint $table) {
			$table->foreign('user_id')->references('id')->on('users')
						->onDelete('restrict')
						->onUpdate('restrict');
		});
		Schema::table('chapters', function(Blueprint $table) {
			$table->foreign('subject_id')->references('id')->on('subjects')
						->onDelete('restrict')
						->onUpdate('restrict');
		});
		Schema::table('paper_categories', function(Blueprint $table) {
			$table->foreign('user_id')->references('id')->on('users')
						->onDelete('restrict')
						->onUpdate('restrict');
		});
		Schema::table('question_types', function(Blueprint $table) {
			$table->foreign('user_id')->references('id')->on('users')
						->onDelete('restrict')
						->onUpdate('restrict');
		});
		Schema::table('question_types', function(Blueprint $table) {
			$table->foreign('paper_category_id')->references('id')->on('paper_categories')
						->onDelete('restrict')
						->onUpdate('restrict');
		});
		Schema::table('questions', function(Blueprint $table) {
			$table->foreign('user_id')->references('id')->on('users')
						->onDelete('restrict')
						->onUpdate('restrict');
		});
		Schema::table('questions', function(Blueprint $table) {
			$table->foreign('paper_category_id')->references('id')->on('paper_categories')
						->onDelete('restrict')
						->onUpdate('restrict');
		});
		Schema::table('questions', function(Blueprint $table) {
			$table->foreign('question_type_id')->references('id')->on('question_types')
						->onDelete('restrict')
						->onUpdate('restrict');
		});
		Schema::table('question_answers', function(Blueprint $table) {
			$table->foreign('user_id')->references('id')->on('users')
						->onDelete('restrict')
						->onUpdate('restrict');
		});
		Schema::table('question_answers', function(Blueprint $table) {
			$table->foreign('paper_category_id')->references('id')->on('paper_categories')
						->onDelete('restrict')
						->onUpdate('restrict');
		});
		Schema::table('question_answers', function(Blueprint $table) {
			$table->foreign('question_id')->references('id')->on('questions')
						->onDelete('restrict')
						->onUpdate('restrict');
		});
		Schema::table('question_mcqs', function(Blueprint $table) {
			$table->foreign('user_id')->references('id')->on('users')
						->onDelete('restrict')
						->onUpdate('restrict');
		});
		Schema::table('question_mcqs', function(Blueprint $table) {
			$table->foreign('question_id')->references('id')->on('questions')
						->onDelete('restrict')
						->onUpdate('restrict');
		});
	}

	public function down()
	{
        Schema::table('users', function(Blueprint $table) {
            $table->dropForeign('users_title_id_foreign');
            $table->dropForeign('users_state_id_foreign');
            $table->dropForeign('users_city_id_foreign');
        });
		Schema::table('classes', function(Blueprint $table) {
			$table->dropForeign('classes_user_id_foreign');
		});
		Schema::table('subjects', function(Blueprint $table) {
			$table->dropForeign('subjects_user_id_foreign');
		});
		Schema::table('subjects', function(Blueprint $table) {
			$table->dropForeign('subjects_class_id_foreign');
		});
		Schema::table('chapters', function(Blueprint $table) {
			$table->dropForeign('chapters_user_id_foreign');
		});
		Schema::table('chapters', function(Blueprint $table) {
			$table->dropForeign('chapters_subject_id_foreign');
		});
		Schema::table('paper_categories', function(Blueprint $table) {
			$table->dropForeign('paper_categories_user_id_foreign');
		});
		Schema::table('question_types', function(Blueprint $table) {
			$table->dropForeign('question_types_user_id_foreign');
		});
		Schema::table('question_types', function(Blueprint $table) {
			$table->dropForeign('question_types_paper_category_id_foreign');
		});
		Schema::table('questions', function(Blueprint $table) {
			$table->dropForeign('questions_user_id_foreign');
		});
		Schema::table('questions', function(Blueprint $table) {
			$table->dropForeign('questions_paper_category_id_foreign');
		});
		Schema::table('questions', function(Blueprint $table) {
			$table->dropForeign('questions_question_type_id_foreign');
		});
		Schema::table('question_answers', function(Blueprint $table) {
			$table->dropForeign('question_answers_user_id_foreign');
		});
		Schema::table('question_answers', function(Blueprint $table) {
			$table->dropForeign('question_answers_paper_category_id_foreign');
		});
		Schema::table('question_answers', function(Blueprint $table) {
			$table->dropForeign('question_answers_question_id_foreign');
		});
		Schema::table('question_mcqs', function(Blueprint $table) {
			$table->dropForeign('question_mcqs_user_id_foreign');
		});
		Schema::table('question_mcqs', function(Blueprint $table) {
			$table->dropForeign('question_mcqs_question_id_foreign');
		});
	}
}