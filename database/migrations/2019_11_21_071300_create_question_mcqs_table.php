<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateQuestionMcqsTable extends Migration {

	public function up()
	{
		Schema::create('question_mcqs', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('user_id')->unsigned()->index();
			$table->integer('question_id')->unsigned()->index();
			$table->string('mcq_option')->nullable();
			$table->string('img')->nullable();
			$table->boolean('is_right')->default(0);
			$table->boolean('status')->default(1);
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('question_mcqs');
	}
}