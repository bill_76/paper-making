<?php

namespace App\Traits;

use App\Models\City;

trait LocationTrait
{
    public function store_city($city)
    {
        $city = City::firstOrCreate(['name' => ucfirst($city)]);
        return $city->id;
    }
}