<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $user_id
 * @property int $question_id
 * @property string $mcq_option
 * @property string $img
 * @property boolean $is_right
 * @property boolean $status
 * @property string $created_at
 * @property string $updated_at
 * @property Question $question
 * @property User $user
 */
class QuestionMcq extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['user_id', 'question_id', 'mcq_option', 'img', 'is_right', 'status', 'created_at', 'updated_at'];

    /**
     * The storage format of the model's date columns.
     * 
     * @var string
     */
    protected $dateFormat = 'U';

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function question()
    {
        return $this->belongsTo('App\Models\Question');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }
}
