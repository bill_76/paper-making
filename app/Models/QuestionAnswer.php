<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $user_id
 * @property int $paper_category_id
 * @property int $question_id
 * @property string $ans
 * @property string $img
 * @property boolean $status
 * @property string $created_at
 * @property string $updated_at
 * @property PaperCategory $paperCategory
 * @property Question $question
 * @property User $user
 */
class QuestionAnswer extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['user_id', 'paper_category_id', 'question_id', 'ans', 'img', 'status', 'created_at', 'updated_at'];

    /**
     * The storage format of the model's date columns.
     * 
     * @var string
     */
    protected $dateFormat = 'U';

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function paperCategory()
    {
        return $this->belongsTo('App\Models\PaperCategory');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function question()
    {
        return $this->belongsTo('App\Models\Question');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }
}
