<?php

namespace App\Http\Controllers;

use App\Http\Requests\UsersRequest;
use App\Models\City;
use App\Models\State;
use App\Models\Title;
use App\Models\User;
use App\Traits\LocationTrait;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Image;
use Spatie\Permission\Models\Role;
use Yajra\DataTables\DataTables;

class UserController extends Controller
{
    use LocationTrait;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $roles = Role::all();

        return view('users', compact('roles'));
    }

    public function show_all(Request $request){

        $user = User::query();

        if($request->ajax()){
            if(isset($request->user_type) && $request->user_type != ''){
                foreach ($request->user_type as $type){
                    $user->role($type);
                }
            }
            if(isset($request->keyword) && $request->keyword != ''){
                $user->orWhere('first_name','like','%'.$request->keyword.'%');
                $user->orWhere('last_name','like','%'.$request->keyword.'%');
                $user->orWhere('phone','like','%'.$request->keyword.'%');
                $user->orWhere('email','like','%'.$request->keyword.'%');
                $user->orWhere('address_line1','like','%'.$request->keyword.'%');
                $user->orWhere('address_line2','like','%'.$request->keyword.'%');
            }
        }
        return Datatables::of($user)
            ->addColumn('name', function ($user) {
                return $user->first_name.' '.$user->last_name;
            })
            ->addColumn('role', function ($user) {
                return clean($user->getRoleNames());
            })
            ->addColumn('status', function ($user) {
                return ($user->is_active)?'<div class="badge badge-success">Active</div>':'<div class="badge badge-danger">Blocked</div>';
            })
            ->addColumn('date', function ($user) {
                return Carbon::parse($user->created_at)->toDayDateTimeString();
            })
            ->addColumn('action', function ($user) {
                return '
                <div class="row">
                    <a href="'.route('users.edit',$user->id).'" class="btn btn-outline-primary btn-sm m-auto mb-md-1 md-sm-1"><i class="ft-edit"></i></a>
                    <a href="#delete-'.$user->id.'" data-id="'.$user->id.'" class="btn btn-outline-danger btn-sm m-auto mb-md-1 md-sm-1"><i class="ft-trash"></i></a>
                </div>
                    
                   ';
            })
            ->make(true);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $cities = City::all();
        $states = State::all();
        $roles = Role::get(['name']);
        $titles = Title::all();

        return view('partials.users.new-user', compact('roles','cities','states','titles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UsersRequest $request)
    {

//        $validator = Validator::make($request->all(), [
//            'first_name' => 'required',
//            'last_name' => 'required',
//            'email' => 'required|email',
//        ]);

        $file_name = $this->file_upload($request);
        $city_id = null;
        if(isset($request->city) && $request->city != '')
            $city_id = $this->store_city($request->city);
        User::create([
            'title_id'          => $request->title_id,
            'is_active'         => 1,
            'city_id'           => $city_id,
            'state_id'          => isset($request->state_id)?$request->state_id:null,
            'first_name'        => $request->first_name,
            'last_name'         => $request->last_name,
            'phone'             => $request->phone,
            'name'              => $request->name,
            'password'          => Hash::make('12345678'),
            'email'             => $request->email,
            'profile_picture'   => $file_name,
            'address_line1'     => $request->address_line1,
            'address_line2'     => $request->address_line2,
        ]);

        return response()->json('Successfully saved!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::findOrFail($id);
        if($user){

            $cities = City::all();
            $states = State::all();
            $roles = Role::get(['name']);
            $titles = Title::all();

            return view('partials.users.update-user', compact('user','roles','cities','states','titles'));
        }

        return redirect()->back()->with('Error','User not exists!');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = User::findOrFail($id);
        if($user) {
            $file_name = $this->file_upload($request);
            $city_id = null;
            if(isset($request->city) && $request->city != '')
                $city_id = $this->store_city($request->city);
            $user->update([
                'title_id' => $request->title_id,
                'is_active' => 1,
                'city_id' => $city_id,
                'state_id' => isset($request->state_id)?$request->state_id:null,
                'first_name' => isset($request->first_name)?$request->first_name:null,
                'last_name' => isset($request->last_name)?$request->last_name:null,
                'phone' => $request->phone,
                'name' => $request->name,
                'password' => Hash::make($request->password),
                'email' => $request->email,
                'profile_picture' => $file_name,
                'address_line1' => $request->address_line1,
                'address_line2' => $request->address_line2,
            ]);

            return response()->json('Successfully saved!');
        }
        return response()->json('');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function file_upload(Request $request)
    {

        $this->validate($request, [
            'avatar' => 'image|required|mimes:jpeg,png,jpg,gif,svg'
        ]);
        $originalImage= $request->file('avatar');
        $thumbnailImage = Image::make($originalImage);
        $thumbnailPath = storage_path().'/app/public/users/thumbnail/';
        $originalPath = storage_path().'/app/public/users/images/';

        if (!file_exists($thumbnailPath)) {
            mkdir($thumbnailPath, 666, true);
        }
        if (!file_exists($originalPath)) {
            mkdir($originalPath, 666, true);
        }

        $fileName = time().'profile.jpg';
        $thumbnailImage->save($originalPath.$fileName);
        $thumbnailImage->resize(300,300);
        $thumbnailImage->save($thumbnailPath.$fileName);

        return $fileName;
    }
}
