<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();


Route::group(['prefix' => 'home', 'as' => 'home'], function () {
    Route::get('', 'HomeController@index');

});

//---------------------------------------- Users Route -------------------------------------------//
Route::group(['prefix' => 'users', 'as' => 'users.'], function () {
    Route::resource('', 'UserController');
    Route::get('/{id}/edit', 'UserController@edit')->name('edit');
    Route::post('/update/{id}', 'UserController@update')->name('update');
    Route::post('/file/upload', 'UserController@file_upload')->name('file.upload');
    Route::get('/show/all', 'UserController@show_all')->name('show.all');
});