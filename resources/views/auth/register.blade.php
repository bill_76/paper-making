@extends('auth.layouts.auth-master')

@section('content')
    <!-- BEGIN: Content-->
    <div class="app-content content content-vcenter bg-image2">
        <div class="content-wrapper">
            <div class="content-body">
                <section class="flexbox-container">
                    <div class="col-12 d-flex align-items-center justify-content-center">
                        <div class="col-lg-4 col-md-6 col-10 box-shadow-2 p-0">
                            <div class="card border-grey border-lighten-3 px-1 py-1 m-0">
                                <div class="card-header border-0">
                                    <div class="text-center mb-1">
                                        <img src="assets/images/logo/logo.png" style="width: auto;height: 26px" alt="branding logo">
                                    </div>
                                    <div class="font-large-1  text-center">
                                        SLIMS Registration
                                    </div>
                                </div>
                                <div class="card-content">

                                    <div class="card-body">
                                        <form method="POST" action="{{ route('register') }}">
                                            @csrf
                                            <fieldset class="form-group position-relative has-icon-left">

                                                <input id="name" type="text"
                                                       class="form-control @error('name') is-invalid @enderror round"
                                                       name="name" value="{{ old('name') }}" required
                                                       placeholder="Username"
                                                       autocomplete="name" autofocus>
                                                <div class="form-control-position">
                                                    <i class="ft-user"></i>
                                                </div>
                                                @error('name')
                                                <span class="invalid-feedback" role="alert">
                                                 <strong>{{ $message }}</strong>
                                                 </span>
                                                @enderror

                                            </fieldset>
                                            <fieldset class="form-group position-relative has-icon-left">
                                                <input id="email" type="email"
                                                       class="form-control @error('email') is-invalid @enderror round"
                                                       name="email" value="{{ old('email') }}" required
                                                       placeholder="E-mail address"
                                                       autocomplete="email">
                                                <div class="form-control-position">
                                                    <i class="ft-mail"></i>
                                                </div>
                                                @error('email')
                                                <span class="invalid-feedback" role="alert">
                                                 <strong>{{ $message }}</strong>
                                                 </span>
                                                @enderror
                                            </fieldset>
                                            <fieldset class="form-group position-relative has-icon-left">
                                                <input id="password" type="password"
                                                       class="form-control @error('password') is-invalid @enderror round"
                                                       name="password" required
                                                       placeholder="Password"
                                                       autocomplete="new-password">
                                                <div class="form-control-position">
                                                    <i class="ft-lock"></i>
                                                </div>
                                                @error('password')
                                                <span class="invalid-feedback" role="alert">
                                                 <strong>{{ $message }}</strong>
                                                 </span>
                                                @enderror
                                            </fieldset>
                                            <fieldset class="form-group position-relative has-icon-left">
                                                    <input id="password-confirm" type="password" class="form-control round"
                                                           name="password_confirmation" required
                                                           placeholder="Confirm password"
                                                           autocomplete="new-password">
                                                <div class="form-control-position">
                                                    <i class="ft-lock"></i>
                                                </div>
                                            </fieldset>
                                            <div class="form-group text-center">

                                                    <button type="submit" class="btn round btn-block btn-glow btn-bg-gradient-x-purple-blue col-12 mr-1 mb-1">
                                                        {{ __('Register') }}
                                                    </button>

                                            </div>
                                        </form>
                                        <div class="card-footer border-0 p-0">
                                            <p class="float-sm-center text-center">
                                                <a href="{{route('login')}}" class="card-link">Back to Login</a>
                                            </p>
                                        </div>
                                    </div>
{{--                                    <p class="card-subtitle line-on-side text-muted text-center font-small-3 mx-2 my-2 ">--}}
{{--                                        <span>OR Sign Up Using</span></p>--}}
{{--                                    <div class="text-center">--}}
{{--                                        <a href="#" class="btn btn-social-icon round mr-1 mb-1 btn-facebook"><span--}}
{{--                                                    class="ft-facebook"></span></a>--}}
{{--                                        <a href="#" class="btn btn-social-icon round mr-1 mb-1 btn-twitter"><span--}}
{{--                                                    class="ft-twitter"></span></a>--}}
{{--                                        <a href="#" class="btn btn-social-icon round mr-1 mb-1 btn-instagram"><span--}}
{{--                                                    class="ft-instagram"></span></a>--}}
{{--                                    </div>--}}

{{--                                    <p class="card-subtitle text-muted text-right font-small-3 mx-2 my-1"><span>Don't have an account ? <a--}}
{{--                                                    href="register.html" class="card-link">Sign Up</a></span></p>--}}
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

            </div>
        </div>
    </div>
    <!-- END: Content-->
{{--<div class="container">--}}
{{--    <div class="row justify-content-center">--}}
{{--        <div class="col-md-8">--}}
{{--            <div class="card">--}}
{{--                <div class="card-header">{{ __('Login') }}</div>--}}

{{--                <div class="card-body">--}}
{{--                    <form method="POST" action="{{ route('login') }}">--}}
{{--                        @csrf--}}

{{--                        <div class="form-group row">--}}
{{--                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>--}}

{{--                            <div class="col-md-6">--}}
{{--                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>--}}

{{--                                @error('email')--}}
{{--                                    <span class="invalid-feedback" role="alert">--}}
{{--                                        <strong>{{ $message }}</strong>--}}
{{--                                    </span>--}}
{{--                                @enderror--}}
{{--                            </div>--}}
{{--                        </div>--}}

{{--                        <div class="form-group row">--}}
{{--                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>--}}

{{--                            <div class="col-md-6">--}}
{{--                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">--}}

{{--                                @error('password')--}}
{{--                                    <span class="invalid-feedback" role="alert">--}}
{{--                                        <strong>{{ $message }}</strong>--}}
{{--                                    </span>--}}
{{--                                @enderror--}}
{{--                            </div>--}}
{{--                        </div>--}}

{{--                        <div class="form-group row">--}}
{{--                            <div class="col-md-6 offset-md-4">--}}
{{--                                <div class="form-check">--}}
{{--                                    <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>--}}

{{--                                    <label class="form-check-label" for="remember">--}}
{{--                                        {{ __('Remember Me') }}--}}
{{--                                    </label>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                        </div>--}}

{{--                        <div class="form-group row mb-0">--}}
{{--                            <div class="col-md-8 offset-md-4">--}}
{{--                                <button type="submit" class="btn btn-primary">--}}
{{--                                    {{ __('Login') }}--}}
{{--                                </button>--}}

{{--                                @if (Route::has('password.request'))--}}
{{--                                    <a class="btn btn-link" href="{{ route('password.request') }}">--}}
{{--                                        {{ __('Forgot Your Password?') }}--}}
{{--                                    </a>--}}
{{--                                @endif--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </form>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}
{{--</div>--}}
@endsection
