<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

@include('auth.layouts.auth-header')

<body class="vertical-layout vertical-menu  bg-full-screen-image " data-open="click" data-menu="vertical-menu" data-color="bg-gradient-x-purple-blue" data-col="1-column">

    @yield('content')

    @include('auth.layouts.auth-scripts')
    @include('layouts.sessionSweetAlerts')

</body>
</html>
