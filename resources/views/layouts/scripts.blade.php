<!-- BEGIN: Vendor JS-->
<script src="{{asset('assets/vendors/js/vendors.min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/vendors/js/forms/toggle/switchery.min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/js/scripts/forms/switch.min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/vendors/js/tables/datatable/datatables.min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/vendors/js/ui/prism.min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/vendors/js/extensions/toastr.min.js')}}" type="text/javascript"></script>
<!-- BEGIN Vendor JS-->

<!-- BEGIN: Page Vendor JS-->
<script src="{{asset('assets/vendors/js/charts/chartist.min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/vendors/js/charts/chartist-plugin-tooltip.min.js')}}" type="text/javascript"></script>
{{--<script src="{{asset('assets/vendors/js/timeline/horizontal-timeline.js')}}" type="text/javascript"></script>--}}
<script src="{{asset('assets/vendors/js/forms/select/select2.full.min.js')}}" type="text/javascript"></script>

{{--<script src="{{asset('assets/vendors/js/tables/datatable/dataTables.bootstrap4.min.js')}}" type="text/javascript"></script>--}}
{{--<script src="{{asset('assets/vendors/js/tables/datatable/dataTables.buttons.min.js')}}" type="text/javascript"></script>--}}
{{--<script src="{{asset('assets/vendors/js/tables/buttons.flash.min.js')}}" type="text/javascript"></script>--}}
{{--<script src="{{asset('assets/vendors/js/tables/jszip.min.js')}}" type="text/javascript"></script>--}}
{{--<script src="{{asset('assets/vendors/js/tables/pdfmake.min.js')}}" type="text/javascript"></script>--}}
{{--<script src="{{asset('assets/vendors/js/tables/vfs_fonts.js')}}" type="text/javascript"></script>--}}
{{--<script src="{{asset('assets/vendors/js/tables/buttons.html5.min.js')}}" type="text/javascript"></script>--}}
{{--<script src="{{asset('assets/vendors/js/tables/buttons.print.min.js')}}" type="text/javascript"></script>--}}
{{--<script src="{{asset('assets/vendors/js/tables/datatable/dataTables.responsive.min.js')}}" type="text/javascript"></script>--}}
{{--<script src="{{asset('assets/vendors/js/tables/datatable/dataTables.rowReorder.min.js')}}" type="text/javascript"></script>--}}
{{--<!-- DATATABLE:-->--}}
{{--<script src="{{asset('assets/js/laravel-datatable.js')}}" type="text/javascript"></script>--}}

<script src="{{asset('assets/vendors/js/pickers/pickadate/picker.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/vendors/js/pickers/pickadate/picker.date.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/vendors/js/pickers/pickadate/picker.time.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/vendors/js/forms/icheck/icheck.min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/vendors/js/animation/jquery.appear.js')}}" type="text/javascript"></script>
{{--<script src="{{asset('assets/vendors/js/scripts/extensions/toastr.min.js')}}" type="text/javascript"></script>--}}
<script src="{{asset('assets/vendors/js/jquery.sharrre.js')}}" type="text/javascript"></script>

<!-- END: Page Vendor JS-->

<!-- BEGIN: Theme JS-->
<script src="{{asset('assets/js/core/app-menu.min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/js/core/app.min.js')}}" type="text/javascript"></script>
<!-- END: Theme JS-->

<!-- BEGIN: Page JS-->
<script src="{{asset('assets/js/scripts/forms/checkbox-radio.min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/js/scripts/forms/select/form-select2.min.js')}}" type="text/javascript"></script>
{{--<script src="{{asset('assets/js/scripts/tables/datatables/datatable-advanced.min.js')}}" type="text/javascript"></script>--}}
{{--<script src="{{asset('assets/js/scripts/tables/datatables/datatable-styling.js')}}" type="text/javascript"></script>--}}
<script src="{{asset('assets/js/scripts/modal/components-modal.min.js')}}" type="text/javascript"></script>
{{--<script src="{{asset('assets/js/scripts/pages/dashboard-ecommerce.min.js')}}" type="text/javascript"></script>--}}
{{--<script src="{{asset('assets/js/scripts/pages/users-contacts.min.js')}}" type="text/javascript"></script>--}}
<script src="{{asset('assets/js/scripts/animation/animation.js')}}" type="text/javascript"></script>
{{--<script src="{{asset('assets/js/scripts/pickers/dateTime/pick-a-datetime.min.js')}}" type="text/javascript"></script>--}}
<script src="{{asset('assets/js/dropzone.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/js/cropper.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/js/jquery.mask.min.js')}}" type="text/javascript"></script>
<script src="{{URL::asset('assets/plugins/sweetalert/sweetalert.min.js')}}"></script>

@include('layouts.common')
@stack('scripts')
<!-- END: Page JS-->
