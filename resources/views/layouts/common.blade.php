
@push('scripts')
    <script type="text/javascript">
        function searchByThis(table) {

            var doneTypingInterval = 800;

            //extraTextMsgCharges();
            clearTimeout(typingTimer);
            typingTimer = setTimeout(function(){  table.draw()}, doneTypingInterval);


        }

        function swalS($msg){
            // swal('Success!', $msg , 'success');
            toastr.success($msg,"Success!")
        }
        function swalI($msg){
            // swal('Info!', $msg , 'info');
            toastr.info($msg,"Info!")
        }
        function swalE($msg){
            // swal('Error!', $msg , 'error');
            toastr.error($msg,"Error!")
        }

        function formSubmit(form_id,btn_id,card_id,blob,callback){

            // formData = new FormData($('#'+form_id));
            // var form = document.getElementById(form_id); // Find the <form> element
            // var formData = new FormData(form); // Wrap form contents

            // form_id.reportValidity();
            var formData = new FormData();
            var formDataArray = $('#'+form_id).serializeArray();
            for(var i = 0; i < formDataArray.length; i++){
                var formDataItem = formDataArray[i];
                formData.append(formDataItem.name, formDataItem.value);
            }

            if(blob && blob !== 'undefined'){
                formData.append('avatar', blob, 'profile.jpg');
            }
            // console.log(blob);
            if(btn_id !== '') {
                $btn = $('#' + btn_id);
                $btn_txt = $btn.html();
                $btn.html($btn.data('loading'));
            }
            if(card_id !== '')
                $('#'+card_id).attr('disabled',true);
            console.log(formData);
            $.ajax({

                method: 'POST',
                url     :$('#'+form_id).attr('action'),
                data    :formData,
                processData: false,
                contentType: false,
                success : function(data){
                    console.log(data);

                    if(btn_id !== '')
                        $btn.html($btn_txt);
                    if(card_id !== '')
                        $('#'+card_id).attr('disabled',false);

                    if(data) {
                        if(callback && typeof callback === "function") {
                            callback(data);
                        }else{
                            if(data.error){
                                swalE(data.error);
                            }else{
                                swalS(data);
                            }

                        }
                    }
                    else {
                        swalE('Something went wrong! please try again.');
                    }
                },
                error: function(data){
                    if(btn_id !== '')
                        $btn.html($btn_txt);
                    // console.log(data.responseJSON.errors);
                    printErrorMsg(data.responseJSON.errors);
                    $(window).scrollTop(0);
                }
            })
        }

        function updateData(form_id,btn_id,params){

            $.each(params, function( index, val ) {
                $('#'+ index).val(val);
            });
            $('#'+ btn_id).text('Update');
            if(modal_id !== '')
                $('#'+modal_id).modal('show');
        }

        function deleteData(url,token,table_id) {

            swal({
                title: "Are you sure?",
                text: "You will not be able to recover this!",
                icon: "warning",
                buttons: [
                    'No, cancel it!',
                    'Yes, I am sure!'
                ],
                dangerMode: true,
            }).then(function (isConfirm) {
                if (isConfirm) {
                    $.ajax({

                        type: "DELETE",
                        url: url,
                        data: {
                            '_token': token
                        },
                        dataType: 'json',
                        success: function (data) {
                            console.log(data);

                            if (table_id !== '')
                                table_id.draw();

                            if (data)
                                swalS(data);
                            else
                                swalE('Something went wrong!')
                        },
                        error: function (data) {

                        }
                    })
                }
            });
        }

        function printErrorMsg (msg) {
            $(".print-error-msg").find("ul").html('');
            $(".print-error-msg").css('display','block');
            $.each( msg, function( key, value ) {
                $(".print-error-msg").find("ul").append('<li>'+value+'</li>');
            });
        }

        function resetForm(form_id) {
            swal({
                title: "Are you sure?",
                text: "You will not be able to recover this!",
                icon: "warning",
                buttons: [
                    'No, cancel it!',
                    'Yes, I am sure!'
                ],
                dangerMode: true,
            }).then(function (isConfirm) {
                if (isConfirm) {
                    var block_ele = $('#' + form_id).closest('.card');

                    // Block Element
                    block_ele.block({
                        message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                        // timeout: 2000, //unblock after 2 seconds
                        overlayCSS: {
                            backgroundColor: '#FFF',
                            cursor: 'wait',
                        },
                        css: {
                            border: 0,
                            padding: 0,
                            backgroundColor: 'none'
                        }
                    });
                    if ($('#' + form_id).trigger("reset")) {
                        $('.select2-reset').val('').trigger('change');
                        block_ele.unblock();
                    }
                }
            });
        }
        //-----------------------------------Assign Data To Modal-----------------------------------//
        function updateModalData(form_id,url = null,method = null,modal_id,params = []){

            if(url){
                $('#'+form_id).attr('action',url)
            }
            if(method){
                $('#'+form_id).attr('method',method)
            }
            $.each(params, function( index, val ) {
                var res = val.split(",");
                $('#'+ index).val(res).trigger('change');
            });
            if(modal_id !== '')
                $('#'+modal_id).modal('show');
        }

        //-----------------------------------Help To Re-Draw Datatables----------------------------------//
        function searchByThis(table) {
            setTimeout( function(){  table.draw()}, 1000);
        }

        //-------------------------------------Form Validation Array-------------------------------------//
        var validate = {
            email: function (input,status) {
                pattern = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
                if(!pattern.test(input.val())){
                    input.addClass('is-invalid');
                    input.children('span').addClass("invalid-select");
                    input.siblings('div.invalid-feedback').html((input.data('field-name') !== 'undefined')? input.data('field-name')+' must be valid.':' must be valid.');
                    status = false;
                }else{
                    input.removeClass('is-invalid');
                    input.addClass('is-valid');
                    // status = true;
                }
                return status;
            },
            phone: function (input,status) {
                pattern = /^(\([0-9]{3}\) |[0-9]{3}-)[0-9]{3}-[0-9]{4}$/g;
                if(!pattern.test(input.val())){
                    input.addClass('is-invalid');
                    input.children('span').addClass("invalid-select");
                    status = false;
                    input.siblings('div.invalid-feedback').html((input.data('field-name') !== 'undefined')? input.data('field-name')+' must be 11 digits.':' must be 11 digits.');
                }else{
                    input.removeClass('is-invalid');
                    input.addClass('is-valid');
                    // status = true;
                }
                return status;
            },
            name: function (input,status) {
                pattern = /^[a-zA-Z\_]{1,50}$/i;
                if(!pattern.test(input.val())){
                    input.addClass('is-invalid');
                    input.children('span').addClass("invalid-select");
                    status = false;
                    input.siblings('div.invalid-feedback').html((input.data('field-name') !== 'undefined')? input.data('field-name')+' must contain alphabets.':' must contain alphabets.');
                }else{
                    input.removeClass('is-invalid');
                    input.addClass('is-valid');
                    // status = true;
                }
                return status;
            },
            name_with_space: function (input,status) {
                pattern = /^[a-zA-Z\_\ ]{1,50}$/i;
                if(!pattern.test(input.val())){
                    input.addClass('is-invalid');
                    input.children('span').addClass("invalid-select");
                    status = false;
                    input.siblings('div.invalid-feedback').html((input.data('field-name') !== 'undefined')? input.data('field-name')+' must contain alphabets.':' must contain alphabets.');
                }else{
                    input.removeClass('is-invalid');
                    input.addClass('is-valid');
                    // status = true;
                }
                return status;
            },
            zip_code: function (input,status) {
                pattern = /^[a-zA-Z0-9\_\-]{1,10}$/i;
                if(!pattern.test(input.val())){
                    input.addClass('is-invalid');
                    input.children('span').addClass("invalid-select");
                    status = false;
                    input.siblings('div.invalid-feedback').html((input.data('field-name') !== 'undefined')? input.data('field-name')+' must be less than 10 characters.':' must be less than 10 characters.');
                }else{
                    input.removeClass('is-invalid');
                    input.addClass('is-valid');
                    // status = true;
                }
                return status;
            },
            digits: function (input,status) {
                pattern = /^[1-9]\d{0,10}$/i;
                if(!pattern.test(input.val())){
                    input.addClass('is-invalid');
                    input.children('span').addClass("invalid-select");
                    status = false;
                    input.siblings('div.invalid-feedback').html((input.data('field-name') !== 'undefined')? input.data('field-name') +' must be digits.':' must be digits.');

                }else{
                    input.removeClass('is-invalid');
                    input.addClass('is-valid');
                    // status = true;
                }
                return status;
            },
            amount: function (input,status) {
                pattern = /[+-]?([0-9]*[.])?[0-9]+/g;
                if(!pattern.test(input.val())){
                    input.addClass('is-invalid');
                    input.children('span').addClass("invalid-select");
                    status = false;
                    input.siblings('div.invalid-feedback').html((input.data('field-name') !== 'undefined')? input.data('field-name')+' must be a valid amount.':' must be a valid amount.');
                }else{
                    input.removeClass('is-invalid');
                    input.addClass('is-valid');
                    // status = true;
                }
                return status;
            },
            date: function (input,status) {
                pattern = /([12]\d{3}-(0[1-9]|1[0-2])-(0[1-9]|[12]\d|3[01]))/i;
                if(!pattern.test(input.val())){
                    input.addClass('is-invalid');
                    input.children('span').addClass("invalid-select");
                    status = false;
                    input.siblings('div.invalid-feedback').html((input.data('field-name') !== 'undefined')? input.data('field-name')+' must be according to format.':' must be according to format.');
                }else{
                    input.removeClass('is-invalid');
                    input.addClass('is-valid');
                    // status = true;
                }
                return status;
            },
            /*time: function (input,status) {
                alert(input.val());
                pattern = /^(0[0-9]|1[0-9]|2[0-3]):[0-5][0-9]$/i;
                if(!pattern.test(input.val())){
                    input.addClass('is-invalid');
                    input.children('span').addClass("invalid-select");
                    status = false;
                    input.siblings('div.invalid-feedback').html((input.data('field-name') !== 'undefined')? input.data('field-name')+' must be according to format.':' must be according to format.');
                }else{
                    input.removeClass('is-invalid');
                    input.addClass('is-valid');
                    // status = true;
                }
                return status;
            }, */
            starttime: function (input,status,endtime) {
                if(input.val() > endtime){
                    input.addClass('is-invalid');
                    input.children('span').addClass("invalid-select");
                    status = false;
                    input.siblings('div.invalid-feedback').html((input.data('field-name') !== 'undefined')? input.data('field-name')+' must be less/equal to End Time.':' must be less/equal to End Time.');
                }else{
                    input.removeClass('is-invalid');
                    input.addClass('is-valid');
                    // status = true;
                }
                return status;
            },
            fromdate: function (input,status,todate) {
                if(input.val() > todate){
                    input.addClass('is-invalid');
                    input.children('span').addClass("invalid-select");
                    status = false;
                    input.siblings('div.invalid-feedback').html((input.data('field-name') !== 'undefined')? input.data('field-name')+' must be less / equal to "To Date".':' must be less / equal to "To Date".');
                }else{
                    input.removeClass('is-invalid');
                    input.addClass('is-valid');
                    // status = true;
                }
                return status;
            }
        };
        masking();
        function masking(){
            $('.money').mask('z', {translation: {
                    'z': {pattern: /^\d*([.\d]+)?$/, recursive: true}
                }}).attr('maxlength', 8);
            $('.date-mask').mask('00/00/0000');
            $('.phone_us').mask('(000) 000-0000');
            $('.time-mask').mask('00:00:00');
            $('.cc_number').mask('0000000000000000');
            $('.cc_cvv').mask('0000');
            $('.cc_expire').mask('00/00');
            $('.phone_with_ddd').mask('0000000000');
            $('.zip-code').mask('00000');
            $('.unit').mask('Z',{translation: {'Z': {pattern: /[a-zA-Z0-9']/, recursive: true}}}).attr('maxlength', 10);
            $('.name').mask('Z',{translation: {'Z': {pattern: /[a-zA-Z\'\.\-\ ]/, recursive: true}}}).attr('maxlength', 56).addClass('capital');
        }

    </script>
@endpush
