<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

@include('layouts.header')

<body class="vertical-layout vertical-menu 2-columns fixed-navbar" data-open="click" data-menu="vertical-menu" data-color="bg-gradient-x-purple-blue" data-col="2-columns">


    @include('layouts.navbars.top')
    @include('layouts.navbars.sidebar')

    @yield('content')

    @include('layouts.footer')
    @include('layouts.scripts')
    @include('layouts.sessionSweetAlerts')

</body>
</html>
