<!-- BEGIN: Main Menu-->
<div class="main-menu menu-fixed menu-light menu-accordion    menu-shadow " data-scroll-to-active="true"
     data-img="{{asset('assets/images/backgrounds/02.jpg')}}">
    <div class="navbar-header">
        <ul class="nav navbar-nav flex-row">
            <li class="nav-item mr-auto"><a class="navbar-brand" href="{{route('home')}}"><img class="brand-logo"
                                                                                        alt="ABC Administration"
                                                                                        src="{{asset('assets/images/logo/logo.png')}}" style="height: auto;width: 190px"/>
{{--                    <h3 class="brand-text">ABC Admin</h3>--}}
                </a></li>
            <li class="nav-item d-md-none"><a class="nav-link close-navbar"><i class="ft-x"></i></a></li>
        </ul>
    </div>
    <div class="navigation-background"></div>
    <div class="main-menu-content">
        <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation">
            <li class="nav-item" id="admin-home"><a href="{{route('home')}}"><i class="ft-monitor"></i><span class="menu-title" data-i18n="">Dashboard</span></a>
            </li>
        </ul>
    </div>
</div>
<!-- END: Main Menu-->
