
<!-- Base style table -->
<div class="row">
    <div class="col-lg-8">
        <select class="select2 form-control select2-placeholder-multiple" onchange="searchByThis(users_table)" id="user-roles" multiple="multiple">

            @foreach($roles as $role)
                <option value="{{$role->name}}">{{$role->name}}</option>
            @endforeach
        </select>
    </div>
    <div class="col-lg-4">
        <fieldset class="form-group">
            <input type="text" id="user-keyword-search" class="form-control" onkeyup="searchByThis(users_table)" placeholder="Search...">
        </fieldset>
    </div>
</div>
<div class="table-responsive">
    <table class="table table-striped table-bordered" id="users-table">
        <thead>
        <tr>
            <th>#</th>
            <th>Name</th>
            <th>Phone</th>
            <th>Email</th>
            <th>Role</th>
            <th>Status</th>
            <th>Date</th>
            <th>Action</th>
        </tr>
        </thead>
    </table>
</div>

<!--/ Base style table -->

@push('scripts')
    <script>
        $('#users-list-item').addClass('active');
    </script>
@endpush
