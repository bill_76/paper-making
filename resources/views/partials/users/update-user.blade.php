@extends('layouts.master')

@section('title','ABC | Users')
@section('content')
    @include('models.users.image-cropper')
    <!-- BEGIN: Content-->
    <div class="app-content content">
        <div class="content-wrapper">
            <div class="content-wrapper-before"></div>
            <div class="content-header row">
                <div class="content-header-left col-md-4 col-12 mb-2">
                    <h3 class="content-header-title"><i class="ft-user-plus"></i> Update User</h3>
                </div>
                <div class="content-header-right col-md-8 col-12">
                    <div class="breadcrumbs-top float-md-right">
                        <div class="breadcrumb-wrapper mr-1">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="">Home</a>
                                </li>
                                <li class="breadcrumb-item"><a href="{{route('users.index')}}">Users</a>
                                </li>
                                <li class="breadcrumb-item active">Update User
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
            <div class="content-body">
                <!-- Base style table -->
                <section id="base-style">
                    <div class="row">
                        <div class="col-8 offset-md-2 offset-sm-0">
                            <div class="card">
                                <div class="card-header">
                                    {{--                                    <h4 class="card-title">Base style</h4>--}}
                                    <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                                    <div class="heading-elements">
                                        <ul class="list-inline mb-0">
                                            {{--                                            <li><a data-action="collapse"><i class="ft-minus"></i></a></li>--}}
                                                                                        <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                            {{--                                            <li><a data-action="expand"><i class="ft-maximize"></i></a></li>--}}
                                            {{--                                            <li><a data-action="close"><i class="ft-x"></i></a></li>--}}
                                        </ul>
                                    </div>
                                </div>
                                <div class="card-content collapse show">
                                    <div class="card-body card-dashboard">
                                        <p class="card-text"><strong>Please enter the details for the new ABC User:</strong></p>
                                        @include('partials.validation.js-validation')
                                        <form class="form" action="{{route('users.update', $user->id)}}" method="PUT" id="update-profile-form">
                                            {{csrf_field()}}
                                            <div class="form-body">
                                                <div class="row">

                                                    <div class="col-md-4 offset-md-8">
                                                        <div class="form-group">
                                                            <label class="label" data-toggle="tooltip" title="Change your Profile Picture">
                                                                <img class="float-right" id="avatar" src="@if($user->profile_picture){{url('storage\\users\\images\\'.$user->profile_picture)}}@else {{asset('assets/images/avatar.png')}} @endif" style="border: 1px solid green; height: 150px">
                                                                <input type="file" class="sr-only" id="input" name="avatar" accept="image/*">
                                                            </label>
{{--                                                            <div class="progress">--}}
{{--                                                                <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100">0%</div>--}}
{{--                                                            </div>--}}
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-2">
                                                        <div class="form-group">
                                                            <label for="title">Title</label>
                                                            <select class="form-control select2-title" id="title" name="title_id">
                                                                <option></option>
                                                                @foreach($titles as $title)
                                                                    <option value="{{$title->id}}" @if($user->title_id == $title->id) selected @endif>{{$title->title}}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="timesheetinput1">First Name</label>
                                                            <div class="position-relative has-icon-left">
                                                                <input type="text" id="timesheetinput1" class="form-control" placeholder="Please enter first name"  value="{{$user->first_name}}" name="first_name">
                                                                <div class="form-control-position">
                                                                    <i class="ft-user"></i>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="timesheetinput2">Last Name</label>
                                                            <div class="position-relative has-icon-left">
                                                                <input type="text" id="timesheetinput2" class="form-control" placeholder="Please enter last name" value="{{$user->last_name}}" name="last_name">
                                                                <div class="form-control-position">
                                                                    <i class="ft-user"></i>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="timesheetinput1">Email Address</label>
                                                            <div class="position-relative has-icon-left">
                                                                <input type="text" id="timesheetinput1" class="form-control" placeholder="Please enter email address" value="{{$user->email}}" name="email">
                                                                <div class="form-control-position">
                                                                    <i class="ft-mail"></i>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="timesheetinput2">Phone Number</label>
                                                            <div class="position-relative has-icon-left">
                                                                <input type="text" id="timesheetinput2" class="form-control" placeholder="Please enter phone number" value="{{$user->phone}}" name="phone">
                                                                <div class="form-control-position">
                                                                    <i class="ft-phone"></i>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="timesheetinput3">Personal Address Line 1</label>
                                                    <div class="position-relative has-icon-left">
                                                        <input type="text" id="timesheetinput3" class="form-control" placeholder="Please enter personal address line 1" value="{{$user->address_line1}}" name="address_line1">
                                                        <div class="form-control-position">
                                                            <i class="ft-home"></i>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="timesheetinput3">Personal Address Line 2</label>
                                                    <div class="position-relative has-icon-left">
                                                        <input type="text" id="timesheetinput3" class="form-control" placeholder="Please enter personal address line 2" value="{{$user->address_line2}}" name="address_line2">
                                                        <div class="form-control-position">
                                                            <i class="ft-home"></i>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="timesheetinput2">City</label>
                                                            <select class="form-control select2-city" id="city-select" name="city">
                                                                <option></option>
                                                                @foreach($cities as $city)
                                                                    <option value="{{$city->name}}" @if(isset($user->city_id) && $user->city->name == $city->name) selected @endif>{{$city->name}}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
{{--                                                    <div class="col-md-4">--}}
{{--                                                        <div class="form-group">--}}
{{--                                                            <label for="timesheetinput1">City</label>--}}
{{--                                                            <div class="position-relative has-icon-left">--}}
{{--                                                                <input type="text" id="timesheetinput1" class="form-control" placeholder="Please enter city" name="employeename">--}}
{{--                                                                <div class="form-control-position">--}}
{{--                                                                    <i class="ft-home"></i>--}}
{{--                                                                </div>--}}
{{--                                                            </div>--}}
{{--                                                        </div>--}}
{{--                                                    </div>--}}
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="timesheetinput2">State</label>
                                                            <select class="form-control select2-states select2-reset" id="default-select" name="state_id">
                                                                <option></option>
                                                                @foreach($states as $state)
                                                                    <option value="{{$state->id}}" @if($user->state_id  == $state->id) selected @endif>{{$state->name}}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
{{--                                                    <div class="col-md-4">--}}
{{--                                                        <div class="form-group">--}}
{{--                                                            <label for="timesheetinput2">State</label>--}}
{{--                                                            <div class="position-relative has-icon-left">--}}
{{--                                                                <input type="text" id="timesheetinput2" class="form-control" placeholder="Please select state" name="projectname">--}}
{{--                                                                <div class="form-control-position">--}}
{{--                                                                    <i class="ft-home"></i>--}}
{{--                                                                </div>--}}
{{--                                                            </div>--}}
{{--                                                        </div>--}}
{{--                                                    </div>--}}
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="timesheetinput2">Zip Code</label>
                                                            <div class="position-relative has-icon-left">
                                                                <input type="text" id="timesheetinput2" class="form-control" placeholder="Please enter zip code" value="{{$user->zip_code}}" name="zip_code">
                                                                <div class="form-control-position">
                                                                    <i class="la la-map-marker"></i>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="timesheetinput2">User Role
                                                                <i class="ft-info" data-toggle="popover" data-content="User Roles allow or deny the user access to certain parts of the application." data-trigger="hover" data-original-title="User Roles"></i>
                                                            </label>
                                                            <select class="form-control select2-roles" id="role-select" name="role">
                                                                <option></option>
                                                                @foreach($roles as $role)
                                                                    <option value="{{$role->name}}">{{$role->name}}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="username">Username</label>
                                                            <div class="position-relative has-icon-left">
                                                                <input type="text" id="username" class="form-control" placeholder="Please enter username" value="{{$user->name}}" name="name">
                                                                <div class="form-control-position">
                                                                    <i class="ft-user"></i>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="password">Password</label>
                                                            <div class="position-relative has-icon-left">
                                                                <input type="text" id="password" class="form-control" placeholder="Please enter password" name="password">
                                                                <div class="form-control-position">
                                                                    <i class="ft-user"></i>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
{{--                                                <div class="form-group">--}}
{{--                                                    <label for="timesheetinput3">Date of Birth</label>--}}
{{--                                                    <div class="position-relative has-icon-left">--}}
{{--                                                        <input type="date" id="timesheetinput3" class="form-control" name="date">--}}
{{--                                                        <div class="form-control-position">--}}
{{--                                                            <i class="ft-message-square"></i>--}}
{{--                                                        </div>--}}
{{--                                                    </div>--}}
{{--                                                </div>--}}

{{--                                                <div class="form-group">--}}
{{--                                                    <label>Rate Per Hour</label>--}}
{{--                                                    <div class="input-group">--}}
{{--                                                        <div class="input-group-prepend">--}}
{{--                                                            <span class="input-group-text">$</span>--}}
{{--                                                        </div>--}}
{{--                                                        <input type="text" class="form-control" placeholder="Rate Per Hour" aria-label="Amount (to the nearest dollar)" name="rateperhour">--}}
{{--                                                        <div class="input-group-append">--}}
{{--                                                            <span class="input-group-text">.00</span>--}}
{{--                                                        </div>--}}
{{--                                                    </div>--}}
{{--                                                </div>--}}
{{--                                                <div class="row">--}}
{{--                                                    <div class="col-md-6">--}}
{{--                                                        <div class="form-group">--}}
{{--                                                            <label for="timesheetinput5">Start Time</label>--}}
{{--                                                            <div class="position-relative has-icon-left">--}}
{{--                                                                <input type="time" id="timesheetinput5" class="form-control" name="starttime">--}}
{{--                                                                <div class="form-control-position">--}}
{{--                                                                    <i class="ft-clock"></i>--}}
{{--                                                                </div>--}}
{{--                                                            </div>--}}
{{--                                                        </div>--}}
{{--                                                    </div>--}}
{{--                                                    <div class="col-md-6">--}}
{{--                                                        <div class="form-group">--}}
{{--                                                            <label for="timesheetinput6">End Time</label>--}}
{{--                                                            <div class="position-relative has-icon-left">--}}
{{--                                                                <input type="time" id="timesheetinput6" class="form-control" name="endtime">--}}
{{--                                                                <div class="form-control-position">--}}
{{--                                                                    <i class="ft-clock"></i>--}}
{{--                                                                </div>--}}
{{--                                                            </div>--}}
{{--                                                        </div>--}}
{{--                                                    </div>--}}
{{--                                                </div>--}}

                                            </div>

                                            <div class="form-actions right">
                                                <button type="button" class="btn btn-danger mr-1">
                                                    <i class="ft-x"></i> Cancel
                                                </button>
                                                <button type="button" id="update-profile-form-btn" onclick="formSubmit('update-profile-form','','',blob_)" class="btn btn-primary">
                                                    <i class="la la-check-square-o"></i> Update
                                                </button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <!--/ Base style table -->
            </div>
        </div>
    </div>
    <!-- END: Content-->

    <!-- HIDDEN: Input fields -->
    <input type="hidden" id="user-type" value="all">

@endsection

@push('scripts')
    <script>
        var canvas;
        let blob_;
        $('.select2-states').select2({
            placeholder: 'Select a State',
            allowClear: true

        });
        $('.select2-title').select2({
            placeholder: 'Select Title',
            allowClear: true

        });




        $('.select2-city').select2({
            tags: true,
            placeholder: 'Select a City',
            allowClear: true
        });
        $('.select2-roles').select2({
            placeholder: 'Select a role for the user',
            allowClear: true

        });
        $('#users-new-item').addClass('active');

        window.addEventListener('DOMContentLoaded', function () {
            var avatar = document.getElementById('avatar');
            var image = document.getElementById('image');
            var input = document.getElementById('input');
            var $progress = $('.progress');
            var $progressBar = $('.progress-bar');
            var $alert = $('.alert');
            var $modal = $('#modal');
            var cropper;

            $('[data-toggle="tooltip"]').tooltip();

            input.addEventListener('change', function (e) {
                var files = e.target.files;
                var done = function (url) {
                    input.value = '';
                    image.src = url;
                    $alert.hide();
                    $modal.modal('show');
                };
                var reader;
                var file;
                var url;

                if (files && files.length > 0) {
                    file = files[0];

                    if (URL) {
                        done(URL.createObjectURL(file));
                    } else if (FileReader) {
                        reader = new FileReader();
                        reader.onload = function (e) {
                            done(reader.result);
                        };
                        reader.readAsDataURL(file);
                    }
                }
            });

            $modal.on('shown.bs.modal', function () {
                cropper = new Cropper(image, {
                    aspectRatio: 1,
                    viewMode: 3,
                });
            }).on('hidden.bs.modal', function () {
                cropper.destroy();
                cropper = null;
            });

            document.getElementById('crop').addEventListener('click', function () {
                var initialAvatarURL;

                $modal.modal('hide');

                if (cropper) {
                    canvas = cropper.getCroppedCanvas({
                        width: 160,
                        height: 160,
                    });
                    initialAvatarURL = avatar.src;
                    avatar.src = canvas.toDataURL();
                    $progress.show();
                    $alert.removeClass('alert-success alert-warning');

                    canvas.toBlob(function (blob) {

                        blob_ = blob;

                        {{--var formData = new FormData();--}}

                        {{--formData.append('avatar', blob, 'avatar.jpg');--}}
                        {{--formData.append('_token', "{{csrf_token()}}");--}}
                        {{--// formData.append($('#update-profile-form :input'));--}}

                        {{--$.ajax("{{route('users.file.upload')}}", {--}}
                        {{--    method: 'POST',--}}
                        {{--    data: formData,--}}
                        {{--    processData: false,--}}
                        {{--    contentType: false,--}}

                        {{--    xhr: function () {--}}
                        {{--        var xhr = new XMLHttpRequest();--}}

                        {{--        xhr.upload.onprogress = function (e) {--}}
                        {{--            var percent = '0';--}}
                        {{--            var percentage = '0%';--}}

                        {{--            if (e.lengthComputable) {--}}
                        {{--                percent = Math.round((e.loaded / e.total) * 100);--}}
                        {{--                percentage = percent + '%';--}}
                        {{--                $progressBar.width(percentage).attr('aria-valuenow', percent).text(percentage);--}}
                        {{--            }--}}
                        {{--        };--}}

                        {{--        return xhr;--}}
                        {{--    },--}}

                        {{--    success: function (data) {--}}
                        {{--        $alert.show().addClass('alert-success').text('Upload success');--}}
                        {{--        console.log(data);--}}
                        {{--    },--}}

                        {{--    error: function () {--}}
                        {{--        avatar.src = initialAvatarURL;--}}
                        {{--        $alert.show().addClass('alert-warning').text('Upload error');--}}
                        {{--    },--}}

                        {{--    complete: function () {--}}
                        {{--        $progress.hide();--}}
                        {{--    },--}}
                        {{--});--}}
                    });
                }
            });
        });
        function updateProfile() {
            console.log(blob_);
        }
    </script>
@endpush
